$(function(){
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    $('.carousel').carousel({
         interval: 2000
    });
    $('#login').on('show.bs.modal', function (e){
        console.log('El modal de login se está mostrando');

        $('.btn-login').removeClass('btn-primary');
        $('.btn-login').addClass('btn-outline-primary');
        $('.btn-login').prop('disabled', true);
    });
    $('#login').on('shown.bs.modal', function (e){
        console.log('El modal de login se mostró');
    });
    $('#login').on('hide.bs.modal', function (e){
        console.log('El modal de login se está ocultando');
    });
    $('#login').on('hidden.bs.modal', function (e){
        console.log('El modal de login se ocultó');

        $('.btn-login').removeClass('btn-outline-primary');
        $('.btn-login').addClass('btn-primary');
        $('.btn-login').prop('disabled', false);
    });

    $('#register').on('show.bs.modal', function (e){
        console.log('El modal de register se está mostrando');

        $('.btn-register').removeClass('btn-primary');
        $('.btn-register').addClass('btn-outline-primary');
        $('.btn-register').prop('disabled', true);
    });
    $('#register').on('shown.bs.modal', function (e){
        console.log('El modal de register se mostró');
    });
    $('#register').on('hide.bs.modal', function (e){
        console.log('El modal de register se está ocultando');
    });
    $('#register').on('hidden.bs.modal', function (e){
        console.log('El modal de register se ocultó');

        $('.btn-register').removeClass('btn-outline-primary');
        $('.btn-register').addClass('btn-primary');
        $('.btn-register').prop('disabled', false);
    });
});